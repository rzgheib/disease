package com.semom.adlsim.Disease;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.semom.MqttPublisherSubscriber;
import org.semom.SemanticSensor;
import org.semom.SparqlReasoning;

public class DiseaseApp  extends MqttPublisherSubscriber {
	
	private int nbOfMessages = 0;
	private HashMap<String, Integer> symptomsIndicators;
	private SemanticSensor Diseasesensor;
	private SymptomsMapping Symptoms;
	
	private SparqlReasoning spR;
	private static String[] topics = {"ActivityLevel", "NumberOfToileting", "DurationOfToileting", 
			"NumberOfMeals", "DurationOfMeals", "NumberOfDrinking", "AmountOfSleep"};
	private String disease = "";
	private HashMap<ArrayList<String>, String> diseaseSymptomsMap;

	public DiseaseApp() throws MqttException, RDFParseException, RepositoryException, IOException {
		
		super("DiseaseClient");
		PropertyConfigurator.configure("log4j_configuration/csparql_readyToGoPack_log4j.properties");
		Symptoms = new SymptomsMapping();
		symptomsIndicators = new HashMap<String, Integer>();
		spR = new SparqlReasoning();
		Diseasesensor = new SemanticSensor();
		Diseasesensor.initialize();
		spR.initialize();
		diseaseSymptomsMap = new HashMap<ArrayList<String>, String>();
		super.runClient();
	}
	
	private void setDisease(String disease) {
		this.disease = disease;
	}
	private String getDisease() {
		return this.disease;
	}

	public void messageArrived(String topicSI, MqttMessage message) throws Exception {
		  
		nbOfMessages++;
		String[] words = message.toString().split("\\)");
		
		int symptomIndexValue = words[1].lastIndexOf(",");
		int apartIndexValue = words[0].lastIndexOf("/");
		int timeIndexValue = words[2].lastIndexOf(",");
		int timeIndexValue2 = words[2].lastIndexOf("\"");
		
		int apart  = Integer.parseInt(words[0].substring(apartIndexValue+1,words[0].length()));
		int symptomIndicatorValue = Integer.parseInt( words[1].substring(symptomIndexValue+3,symptomIndexValue+4 ));
		Long resultTime = Long.parseLong(words[2].substring(timeIndexValue+3,timeIndexValue2 ));
		
	
		/*Date date = new Date(resultTime);
	    Format format = new SimpleDateFormat("yyyy MM dd");
		System.out.println("SymptomIndicationAlertReceived " +
				format.format(date) + " apartment" + " " + apart);*/
		
		symptomsIndicators.put(topicSI, symptomIndicatorValue);		
	    
		System.out.println("Recieved Message :: -----------------------------");
	    System.out.println("| Topic:" + topicSI);
	    System.out.println("| Message: " + new String(message.getPayload()));
	    System.out.println("End ---------------------------------------");
	  
	    if (nbOfMessages % 7 ==0) {
	       //System.out.println("symptoms Indicators 7 : " + symptomsIndicators.values());	     	
	       // SI: [NumberOfDrinking, NumberOfMeals, DurationOfToileting, DurationOfMeals, AmountOfSleep, NumberOfToileting, ActivityLevel]	    	
	     	ArrayList<String> symptomsList = Symptoms.findSymptoms(symptomsIndicators);  
	     	
	        
		   	for (ArrayList<String> key : diseaseSymptomsMap.keySet()) {	     		
	     		if (key.toString().contentEquals(symptomsList.toString()))
	     	    	setDisease(diseaseSymptomsMap.get(key));
	     	  //System.out.println("key : " + key + " symptomsList: " + symptomsList);
	     	}
		   		     	
	     	if(this.disease=="" && symptomsList.size()>1) {
	     		BindingSet diseaseSet = findDisease(symptomsList);
	    		if (diseaseSet != null){	    		
		    		Pattern p = Pattern.compile("\"([^\"]*)\"");
		    	    Matcher m = p.matcher(diseaseSet.toString());		    	    
		    	    while (m.find()) {
		    	    	  setDisease ( m.group(1));
		    	    }
	    		}
	     	}
	     	
	     	if(this.disease!="" && apart!= 0) {
	     		//System.out.println("Disease is: " + getDisease() + " in apartement " + apart
		    		//	+ "at day: " +resultTime);		  	    	
		  	    diseaseSymptomsMap.put(symptomsList, getDisease());
	     		Model Indicator1 = Diseasesensor.addDiseaseObservation(apart, "Disease", resultTime, getDisease()); 
	     		this.sendMessage("Disease", Indicator1.toString());
	     		setDisease("");
	     	}
   	     }
	    if (nbOfMessages % 16546 ==0)
	    	System.out.println("SPARQL durations:  "+spR.getDuration());
	    /*if (nbOfMessages % 24276 ==0) {
	    	stopClient();
	    	//Runtime.getRuntime().gc();
	      }*/
	  }

		 
	public BindingSet findDisease(ArrayList<String> symptoms){
		spR.defineQuery(symptoms);		
		 return spR.runQuery();
	}
		
	public static void main(String[] args) throws RDFParseException, RepositoryException, IOException, MqttException, InterruptedException{
    	DiseaseApp App = new DiseaseApp();
    	App.subscribeTO(topics);
    }

}
