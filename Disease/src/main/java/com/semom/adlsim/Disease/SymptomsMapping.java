package com.semom.adlsim.Disease;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.PropertyConfigurator;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.semom.MqttPublisherSubscriber;

import eu.larkc.csparql.core.engine.ConsoleFormatter;
import eu.larkc.csparql.core.engine.CsparqlEngineImpl;
import eu.larkc.csparql.core.engine.CsparqlQueryResultProxy;

public class SymptomsMapping{
	
	private String queryBody;
	private CsparqlEngineImpl engine;
	private CsparqlQueryResultProxy c;

	public SymptomsMapping() {		
	}
	
	public SymptomsMapping(String CSPARQL) throws MqttException {

		PropertyConfigurator.configure("log4j_configuration/csparql_readyToGoPack_log4j.properties");
		
	}

	// symptomIndicators:[NumberOfDrinking, NumberOfMeals, DurationOfToileting, DurationOfMeals,
	//AmountOfSleep, NumberOfToileting, ActivityLevel]
	
public ArrayList<String> findSymptoms(HashMap<String, Integer> symptomIndicatotrs){
		
		ArrayList<String> symptoms = new ArrayList<String>(); 
		List<Integer> values = new ArrayList<Integer>();
		//String[] symptoms = {"diarrhea", "vomiting", "loss of appetite", nausea};	
		for (Map.Entry SI : symptomIndicatotrs.entrySet()){
			values.add((Integer)SI.getValue());
		}

		//if (values.get(1)<3 || values.get(1) > 3 || values.get(3)<3 || values.get(3) > 3) //NumberOfMeals
			//symptoms.add("SYMP:0000309"); //loss of appetite
		
		//if (values.get(3)<3 || values.get(3) > 3 ) //NumberOfMeals
			//symptoms.add("SYMP:0000309"); //loss of appetite
					
		if(values.get(0)<3 || values.get(1)<3 || values.get(3)<3) { //eating drinking
			symptoms.add("SYMP:0019145"); //vomiting
			symptoms.add("SYMP:0000458"); //nausea
		}
		
		if (values.get(2)>3 ) //durationOfToileting
			symptoms.add("SYMP:0000570"); //diarrhea
		
		if(values.get(5)>3) //NumberOfToileting
			symptoms.add("SYMP:0019145"); //vomiting
		
		if(values.get(6)<3 || values.get(4)<3 ) //ActivityLevel	and sleep very low
			symptoms.add("SYMP:0019177"); //fatigue		

		return symptoms;

	}

	public ArrayList<String> findSymptomsMapp(HashMap<String, Integer> symptomIndicatotrs){
		
		ArrayList<String> symptoms = new ArrayList<String>(); 
		List<Integer> values = new ArrayList<Integer>();
		//String[] symptoms = {"diarrhea", "vomiting", "loss of appetite", nausea};	
		for (Map.Entry SI : symptomIndicatotrs.entrySet()){
			values.add((Integer)SI.getValue());
		}
		
		if(values.get(1)>3 || values.get(2)>3 || values.get(3)>3 ||
				values.get(4)>3 || values.get(5)>3 || values.get(6)>3) {
			symptoms.add("SYMP:0019145");
			symptoms.add("SYMP:0000570");
			symptoms.add("SYMP:0000458");
			symptoms.add("SYMP:0019177");			
		}
		
		if(values.get(1)<3 || values.get(2)<3 || values.get(3)<3 ||
				values.get(4)<3 || values.get(5)<3 || values.get(6)<3) {
			symptoms.add("SYMP:0019145");
			symptoms.add("SYMP:0000570");
			symptoms.add("SYMP:0000458");
			symptoms.add("SYMP:0019177");			
		}
		
		return symptoms;

	}


}
				/*
		switch (symptomIndicator){
			case "ActivityLevel":  
				if (symptomIndicatorValue < 3){
					//symptomsList[i] = "Discomfort";
					symptom = "nausea";
					//System.out.println("symptomList 1 : " + symptomsList[i]);
					//i++;
					}
	        break;
			case "AmountOfSleep":  
				if (symptomIndicatorValue < 5){
					//symptomsList[i] = "Discomfort";
					symptom = "Discomfort2";
					//System.out.println("symptomList 2 : " + symptomsList[i]);
					//i++;
					}
	        break;
			case "NumberOfMeals":  
				if (symptomIndicatorValue < 5){
					symptom = "loss of appetite1";
					//symptomsList[i] = "loss of appetite";
					//i++;
					}
	        break;
			case "DurationOfMeals":  
				if (symptomIndicatorValue < 5){
					symptom = "loss of appetite2";
					//symptomsList[i] = "loss of appetite";
					//i++;
					}
	        break;
			case "NumberOfToileting":  
				if (symptomIndicatorValue > 3){
					symptom = "diarrhea";
					//symptomsList[i] = "diarrhea";
					//i++;
					}
	        break;
			case "DurationOfToileting": 
				if (symptomIndicatorValue > 3){
					symptom = "vomiting";
					//symptomsList[i] = "vomiting";
					//i++;
					}
	        break;
			case "NumberOfDrinking": 
				if (symptomIndicatorValue < 5){
					symptom = "loss of appetite3";
					//symptomsList[i] = "loss of appetite";
					//i++;
					}
	        break;
						
		}*/

		
